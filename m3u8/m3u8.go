package m3u8

import (
	"fmt"
	"os"
	"strconv"

	"HLSScrapper/m3u8/dl"
)

var (
	output   string
	chanSize int
)

func Cookbook(url string, num int) {
	chanSize = 25
	output = "out"

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("[error]", r)
			os.Exit(-1)
		}
	}()

	downloader, err := dl.NewTask(output, strconv.Itoa(num)+".mp4", url)
	if err != nil {
		panic(err)
	}
	if err := downloader.Start(chanSize); err != nil {
		panic(err)
	}
	fmt.Println("Done!")
}

func panicParameter(name string) {
	panic("parameter '" + name + "' is required")
}
