package main

import (
	"HLSScrapper/m3u8"
	"bufio"
	"github.com/ericchiang/css"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {
	urlsFile := safeExec1("urls.txt", os.Open)

	defer urlsFile.Close()

	scanner := bufio.NewScanner(urlsFile)

	i := 1
	for scanner.Scan() {
		url := strings.TrimSpace(scanner.Text())

		if url != "" {
			downloadVideo(url, i)
			i++
		}
	}
}

func downloadVideo(rootUrl string, num int) {

	resp := safeExec1(rootUrl, http.Get)
	defer safeExecNoRet0(resp.Body.Close)

	sel := css.MustParse("#vgc-player")

	node := safeExec1(resp.Body.(io.Reader), html.Parse)

	for _, elem := range sel.Select(node) {
		processPlayer(elem, num)
	}
}

func processPlayer(node *html.Node, num int) {
	var rawPlaylistUrl string

	for _, attr := range node.Attr {
		if attr.Key == "data-master" {
			rawPlaylistUrl = attr.Val
		}
	}

	if rawPlaylistUrl == "" {
		panic("NO URL")
	}

	m3u8.Cookbook(rawPlaylistUrl, num)
}

func safeExecNoRet0(f func() error) {
	err := f()
	if err != nil {
		panic(err)
	}
}

func safeExecNoRet1[T1 any](arg T1, f func(arg T1) error) {
	err := f(arg)
	if err != nil {
		panic(err)
	}
}

func safeExec1[K any, T1 any](arg T1, f func(arg T1) (K, error)) K {
	res, err := f(arg)
	if err != nil {
		panic(err)
	}
	return res
}

func safeExec2[K any, T1 any, T2 any](arg1 T1, arg2 T2, f func(arg1 T1, arg2 T2) (K, error)) K {
	res, err := f(arg1, arg2)
	if err != nil {
		panic(err)
	}
	return res
}
